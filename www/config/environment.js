/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'open-ethereum-pool',
    environment: environment,
    rootURL: '/',
    locationType: 'hash',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // PoolName
      PoolName: 'ΞGEM',
      // PoolTitle
      PoolTitle: 'Open ΞGEM Pool',
      // PoolLink
      PoolLink: 'https://pool.egem.io',
      // API host and port
      ApiUrl: '//pool.egem.io/',
      // ExchangeLinks
      STEXLink: 'https://www.stex.com/',
      RAISELink: 'https://raisex.io/',
      FCBLink: 'https://exfcb.com/',
      ESCODEXLink: 'https://www.escodex.com/',
      GraviexLink: 'https://graviex.net/',
      MRRLink: 'https://www.miningrigrentals.com',
      NHLink: 'https://www.nicehash.com/',

      // HTTP mining endpoint
      HttpHost: 'http://pool.egem.io/',
      HttpPort: 8888,

      // Stratum mining endpoint
      StratumHost: 'pool.egem.io',
      StratumPort: 8008,

      // Fee and payout details
      PoolFee: '1%',
      PayoutThreshold: '1 EGEM',
      PayoutInterval: '60m',
      Unit: 'EGEM',
      EtherUnit: 'EGEM',

      // For network hashrate (change for your favourite fork)
      BlockExplorerLink: 'https://explorer.egem.io',
      BlockExplorerAddrLink: 'https://explorer.egem.io/addr',
      DonationLink: false,
      DonationAddress: '',
      BotAddress: '0xd66f71aa3e24acfcb2e96eedf3d2516711d95d88',
      BlockReward: 4,
      BlockTime: 13
    }
  };

  if (environment === 'development') {
    /* Override ApiUrl just for development, while you are customizing
      frontend markup and css theme on your workstation.
    */
    ENV.APP.ApiUrl = 'http://localhost:8080/'
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
